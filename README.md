# Black Desert Console - Donkey Race
**Event Date:** Friday, March 1st / 2024

## Getting started

## Race Rules
1. Everyone can participate without any restrictions with beginners and veteran players | with or without guilds.

2. The first **38 players** who send **X** in the general/server chats will be in the **1st and 2nd platoons** to compete in the race. Where the image of this platoons will be sent to the in-game CM/GMs for distribution of future prizes.

3. Donkey race participants will need to go to **NPC Shai – Gula** in _Stonetail Horse Rach_
and buy a **Donkey Emblem: Strong Black Donkey**.

#### DONKEY RACE - PRIZES
- [ ] **1st:** [Event] Blessing of Old Moon Pack (7 Days) + GMs Care 2
- [ ] **2nd:** [Event] Blessing of Old Moon Pack (7 Days) + GMs Care 1
- [ ] **3rd:** GMs Care 1 + GMs Care 2
- [ ] **4th:** GMs Care 2
- [ ] **5th:** GMs Care 1
- [ ] **Participation:** Item Collection Increase Scroll x2

**Supporters:** [CM] **Durgeff**, other [GM] and Ambassadors who will be able to perform on the day of the event.
* **Notice:** Any type of cheating will result in the competitor being disqualified from the race and removed from the platoon in which they are competing.

| IMPORTANT: |
| ------ |
|    - You will **NOT** be able to use the **SHAI** class. Because it has a buff to make the donkey run.    |
